# build base image for GPU based clusters
Extend basic image(s) from [docker-tensorflow](https://hub.docker.com/r/tensorflow/tensorflow/tags)
Basic structure is given as:

- workdir in /tf, that contains /tf/tutorials
- mount local (SSD) share under /tf/local
- mount local directory of choice under /tf/data (default at /home/<user>/Documents)

Adds ``lsr_gpu_docker``` as an executable to the system that allows normal users to call script as needed

Upon first installation add

```
%docker ALL=NOPASSWD:/usr/local/bin/lsr_gpu_docker
```

to ```/etc/sudoers``` by runnign ```visudo```.
