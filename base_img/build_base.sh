#!/bin/bash
# admin script -> not for default users
# requests in base image can be add to
JUPY_DIR="$(pwd)/.jupyter"
function set_default_jupyter_conf(){
if [ -z ${1} ]; then echo "please provide fille name"; else
echo "# ================================================
# Default jupyter config
# ================================================
import os
from IPython.lib import passwd

c = c  # pylint:disable=undefined-variable
c.NotebookApp.ip = '0.0.0.0'
c.NotebookApp.keyfile = os.getenv('SSLKEY', '/etc/ssl/jupyter/mycert.pem')
c.NotebookApp.certfile = os.getenv('SSLCERT','/etc/ssl/jupyter/mycert.pem')
c.NotebookApp.port = int(os.getenv('PORT', 8888))
c.NotebookApp.open_browser = False

# sets a password if PASSWORD is set in the environment
c.NotebookApp.allow_password_change=True
if 'PASSWORD' in os.environ:
  password = os.environ['PASSWORD']
  if password:
    c.NotebookApp.password = passwd(password)
  del os.environ['PASSWORD']

# Extensions
c.NotebookApp.nbserver_extensions.jupyter_nbextensions_configurator =  True
c.NotebookApp.nbserver_extensions.jupyterlab =  True

" > ${1}
fi
}

function set_jupy_conf(){
	CUR_DIR=$(pwd)
	if [ ! -d "${JUPY_DIR}" ]; then
		mkdir "${JUPY_DIR}"
		set_default_jupyter_conf "${JUPY_DIR}/jupyter_notebook_config.py"
	fi
	if [ ! -f "${JUPY_DIR}/jupyter_notebook_config.py" ]; then
		set_default_jupyter_conf "${JUPY_DIR}/jupyter_notebook_config.py"
	fi
	cd ${CUR_DIR}
}

function build_version_image(){
	if [ -z ${1} ]; then VERSION="latest"; else VERSION="${1}";fi
	echo "FROM tensorflow/tensorflow:${VERSION}-gpu-py3-jupyter" | tee  Dockerfile
	cat Dockerfile.head >> Dockerfile
	echo 'CMD ["jupyter", "notebook"]' >> Dockerfile
	docker build -t volkg/gpu-py3-jupy:base-tf-${VERSION} .
	docker push volkg/gpu-py3-jupy:base-tf-${VERSION}
	echo "FROM tensorflow/tensorflow:${VERSION}-gpu-py3-jupyter" | tee  Dockerfile
	cat Dockerfile.head >> Dockerfile
	cat Dockerfile.lab >> Dockerfile
	echo 'CMD ["jupyter", "lab"]' >> Dockerfile
	docker build -t volkg/gpu-py3-jupy:base-lab-tf-${VERSION} .
	docker push volkg/gpu-py3-jupy:base-lab-tf-${VERSION}
}
docker login
set_jupy_conf
build_version_image "1.14.0"
build_version_image "1.15.2"
build_version_image
rm -r ${JUPY_DIR}

