#!/bin/bash
# author: v.gabler@tum.de
# wrapper to allow limited docker access to all users
# in case you built your own image using the docker commands, please comment below

SSL_DIR="$(pwd)/ssl"
JUPY_DIR="${HOME}/.jupyter_docker"
IMAGE_NAME="${USER}_image"
DATA="${HOME}/Documents"
LOCAL_DATA="/home/$(whoami)/local_$(hostname)"

function set_jupy_conf(){
	CUR_DIR=$(pwd)
	if [ ! -d "${SSL_DIR}" ]; then 
		mkdir "${SSL_DIR}"  
	fi
	cd "${SSL_DIR}"
	openssl req -newkey rsa:4096 -x509 -sha256  -days 3650  -nodes -out mycert.pem -keyout mycert.pem \
                       -subj "/C=DE/ST=Bavaria/L=Munich/O=TUM/OU=LSR/CN=$(hostname)"
	cd ${CUR_DIR}
	if [ ! -d "${HOME}/.local_docker" ]; then
		mkdir "${HOME}/.local_docker"
	fi
}

function run(){
	echo "Warning: Softlink will not work within this script. Please check if:
	- ${DATA}
	- ${HOME}
	- ${LOCAL_DATA}
	contain any softlinks"
	PORT="$(echo 2$(cat /dev/urandom | tr -dc '0-9' | fold -w 256 | head -n 1 | sed -e 's/^0*//' | head --bytes 4))"
	VIRTUAL_HOME="/home/coder"
	echo "You can access your notebook at 'https://$(hostname).lsr.ei.tum.de:${PORT}'"
	if [  -z ${1} ]; then
    {
	    docker run --gpus all -p "${PORT}":8888  --name ${USER}_container \
				-v "${DATA}":"${VIRTUAL_HOME}/data" \
				-v "${SSL_DIR}":"/etc/ssl/jupyter" \
				-v "${HOME}/.local_docker":"${VIRTUAL_HOME}/.local" \
				-v "${LOCAL_DATA}":"${VIRTUAL_HOME}/local" \
				--rm  ${IMAGE_NAME}
            } || {
        docker run  -p "${PORT}":8888 --name ${USER}_container \
				-v "${DATA}":"${VIRTUAL_HOME}/data" \
				-v "${SSL_DIR}":"/etc/ssl/jupyter" \
				-v "${HOME}/.local_docker":"${VIRTUAL_HOME}/.local" \
				-v "${LOCAL_DATA}":"${VIRTUAL_HOME}/local" \
				--rm  ${IMAGE_NAME}
    }
	fi
}


function usage(){
    echo "use this file to build or run a docker container for tensorflow. Options to use:"
    echo " - build: builds docker image named ${IMAGE_NAME} according to the rules defined in 'Dockerfile'"
    echo " - run: Spawns a docker container, whith the following data aquistions:"
    echo "  -> your local data ${LOCAL_DATA} is mounted to 'local'"
    echo "  -> the data directory ${DATA} is mounted to 'data'"
    echo " - kill:  kill current docker container "
    echo " - rm: remove current docker container"
}

case ${1} in
	build)
		docker build -t ${IMAGE_NAME} \
		       	--build-arg U_ID=$(id -u) --build-arg G_ID=$(id -g) \
   		      .
		;;
	run)
		set_jupy_conf
		docker kill "${USER}_container"
		run ${2}
		;;
	kill)
		docker kill "${USER}_container"
		;;
	rm)
		docker rm "${USER}_container"
		;;
	*)
		usage
		;;
esac

